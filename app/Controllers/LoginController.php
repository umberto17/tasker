<?php

namespace App\Controllers;

use App\Common\DB;
use App\Common\Request;
use App\Common\Response;
use App\Interfaces\ControllerInterface;
use App\Models\User;

class LoginController implements ControllerInterface
{
    function getAll()
    {
        Response::getInstance()->notFound();
    }

    function get()
    {
        Response::getInstance()->notFound();
    }

    function post()
    {
        $payload = Request::getInstance()->payload;
        $response = Response::getInstance();
        if ($user = (new User())->getByEmailPwd($payload["email"], $payload["password"])) {
            $key = DB::generateKey();
            (new User())->updateKey($user->id, $key);
            $user->key = $key;
            $response->resOk(["user" => $user]);
        } else {
            $response->unAuth();
        }
    }

    function put()
    {
        Response::getInstance()->notFound();
    }

    function delete()
    {
        Response::getInstance()->notFound();
    }
}
