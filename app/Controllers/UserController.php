<?php

namespace App\Controllers;

use App\Common\Request;
use App\Interfaces\ControllerInterface;
use App\Common\Response;
use App\Models\User;
use App\Common\DB;

class UserController implements ControllerInterface {
    function getAll() {
        Response::getInstance()->forbidden();
    }

    function get() {
        $response = Response::getInstance();
        if ($user = Request::getInstance()->currentUser) {
            $response->resOk(["user" => $user]);
        } else {
            $response->forbidden();
        }
    }

    function post() {
        $data = Request::getInstance();
        if (isset($data->payload)) {
            $data = $data->payload;
        } else {
            Response::getInstance()->badRequest();
        }
        $password_confirm = $data["password_confirm"];
        if (
            DB::validate("email", $data["email"]) &&
            DB::validate("password", $data["password"]) &&
            $data["password"] === $password_confirm
        ) {
            $toInsert = [
                $data["email"],
                DB::password($data["password"])
            ];
            $user = new User();
            $response = Response::getInstance();
            if ($user->checkForEmail($data['email'])) {
                $response->badRequest("Already exists", 409);
            } else if ($user->insert($toInsert)) {
                $response->resOk([], "User created", 201);
            }
        } else {
            Response::getInstance()->badRequest();
        }
    }

    function put() {
        Response::getInstance()->forbidden();
    }

    function delete() {
        Response::getInstance()->forbidden();
    }
}
