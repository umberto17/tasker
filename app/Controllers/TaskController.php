<?php

namespace App\Controllers;

use App\Common\DB;
use App\Common\Request;
use App\Common\Response;
use App\Interfaces\ControllerInterface;
use App\Models\SubTask;
use App\Models\Task;

class TaskController implements ControllerInterface {
    function getAll()
    {
        $response = Response::getInstance();
        if ($tasks = (new Task())->getAll()) {
            $response->resOk((new SubTask)->joinSubtasks($tasks));
        } else {
            $response->badRequest();
        }
    }

    function get()
    {
        $response = Response::getInstance();
        if ($tasks = (new Task())->get()) {
            $response->resOk($tasks);
        } else {
            $response->badRequest();
        }
    }

    function post()
    {
        $request = Request::getInstance();
        $data = $request->payload;
        $user = $request->currentUser;

        $toInsert = [
            "date" => $data["date"],
            "title" => $data["title"],
            "description" => $data["description"],
            "completed" => $this->getCompleted($data["subtasks"]),
            "user_id" => $user->id
        ];
        if (
            DB::validate("date", $toInsert["date"]) &&
            DB::validate("string", $toInsert["title"]) &&
            DB::validate("string", $toInsert["description"])
        ) {
            if ($task = (new Task)->insert($toInsert)) {
                if (isset($data["subtasks"]) && is_array($data["subtasks"])) {
                    $this->updateSubtasks($data["subtasks"], DB::lastId("tasks"));
                }
                Response::getInstance()->resOk([], "Task created", 201);
            }
        } else {
            Response::getInstance()->badRequest();
        }
    }

    function put()
    {
        $request = Request::getInstance();
        $response = Response::getInstance();
        $data = $request->payload;

        $toUpdate = [
            "date" => $data["date"],
            "title" => $data["title"],
            "description" => $data["description"],
            "completed" => $this->getCompleted($data["subtasks"], $data)
        ];
        if (
            DB::validate("date", $toUpdate["date"]) &&
            DB::validate("string", $toUpdate["title"]) &&
            DB::validate("string", $toUpdate["description"])
        ) {
            if ((new Task())->update($toUpdate)) {
                if (isset($data["subtasks"]) && is_array($data["subtasks"])) {
                    $this->updateSubtasks($data["subtasks"], $request->id);
                }
                $response->resOk([], "Task updated", 201);
            }
        } else {
            $response->badRequest();
        }
    }

    function delete()
    {
        $response = Response::getInstance();
        if ((new Task())->delete()) {
            $response->resOk([], "Deleted", 201);
        } else {
            $response->badRequest();
        }
    }

    private function getCompleted(array $subtasks, $task = 0)
    {
        $checkForComplete = array_filter($subtasks, function ($subtask) {
            return !isset($subtask["delete"]);
        });

        if (!count($checkForComplete)) {
            return $task["completed"];
        }

        return !count(array_filter($subtasks,function ($subtask) {
            return !$subtask["completed"];
        }));
    }

    private function updateSubtasks(array $subtasks, $id)
    {
        array_walk($subtasks, function ($subtask) use ($id) {
            $prepared = [
                "title" => $subtask["title"],
                "completed" => $subtask["completed"] ? 1 : 0,
                "task_id" => $id
            ];
            if (isset($subtask["id"]) && !isset($subtask["delete"])) {
                (new SubTask)->update($prepared, $subtask["id"]);
            } elseif (isset($subtask["delete"]) && $subtask["delete"]) {
                if ($subtask["id"]) {
                    (new SubTask)->delete($subtask["id"]);
                }
            } else {
                (new SubTask)->insert($prepared);
            }
        });
    }
}