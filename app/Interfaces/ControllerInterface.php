<?php

namespace App\Interfaces;

interface ControllerInterface {
    function getAll();
    function get();
    function post();
    function put();
    function delete();
}
