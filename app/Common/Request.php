<?php

namespace App\Common;

use App\Models\User;

class Request {
    private static $instance;
    private $protected = ["Task"];

    public $currentUser;
    public $method;
    public $object;
    public $id;
    public $payload;

    private function __construct() {
        $this->handleHeaders();
        $this->handleMethod();
        $this->handleUrl();
        $this->handlePayload();
    }

    private function __clone() {}

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    private function handleHeaders()
    {
        $origin = (APP_MODE === "prod") ? APP_ORIGIN : "*";
        header("Access-Control-Allow-Origin: {$origin}");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Headers: Authorization, Content-Type");
        header("Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS");

        $headers = getallheaders();
        $authKey = (isset($headers['Authorization'])) ? $headers['Authorization'] : null;
        $this->currentUser = $authKey ? (new User())->getByKey($authKey) : null;
    }

    private function handleMethod()
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case "OPTIONS": exit; break;
            default:
                $this->method = strtolower($_SERVER['REQUEST_METHOD']);
                break;
        }
    }

    private function handleUrl()
    {
        if (preg_match("/^\/([a-zA-Z]*)(\/(\d+))?\/?$/", $_SERVER["REQUEST_URI"], $matches)) {
            $this->object = ucfirst($matches[1]);
            $this->id = isset($matches[3]) ? $matches[3] : null;

            if (array_key_exists($this->object, array_flip($this->protected)) && !$this->currentUser) {
                Response::getInstance()->forbidden();
            }
        } else {
            Response::getInstance()->notFound();
        }

    }

    private function handlePayload()
    {
        $this->payload = json_decode(file_get_contents("php://input"), true);
    }
}
