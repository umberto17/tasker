<?php

namespace App\Common;

class DB {
    private static $instance;

    private static $getOneQuery = "select * from ? where ?";

    private static $insert = "insert into ? (?) values (?)";
    private static $update = "update ? set ? where ?";

    private static $migrationsPath = __DIR__ . "/../../sql";

    private function __construct() {}
    private function __clone() {}

    private static function connect($host, $db, $user, $pwd)
    {
        try {
            self::$instance = new \PDO("mysql:dbname={$db};host={$host}", $user, $pwd);
        } catch (\PDOException $e) {
            print_r($e);
            die("Connection failed: {$e->errorInfo}");
        }
    }

    private static function getInstance()
    {
        if (!self::$instance) {
            self::connect(DB_SERVER, DB_NAME, DB_USERNAME, DB_PASSWORD);
        }
        return self::$instance;
    }

    public static function get($data, $preSql)
    {
        $pdo = self::getInstance();
        $state = $pdo->prepare($preSql);
        $state->execute($data);
        return $state->fetchObject();
    }

    public static function getAll($data, $preSql)
    {
        $pdo = self::getInstance();
        $state = $pdo->prepare($preSql);
        $state->execute($data);
        print_r($state->errorInfo);
        return $state->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function insert($data, $preSql)
    {
        $pdo = self::getInstance();
        $state = $pdo->prepare($preSql);
        return $state->execute(array_values($data));
    }

    public static function update($data, $preSql)
    {
        $pdo = self::getInstance();
        $state = $pdo->prepare($preSql);
        return $state->execute(array_values($data));
    }

    public static function delete($data, $preSql)
    {
        $pdo = self::getInstance();
        $state = $pdo->prepare($preSql);
        return $state->execute($data);
    }

    public static function validate($type, $value) {
        switch ($type) {
            case "number":
                return is_numeric($value);
            case "email":
                return preg_match("/^[a-zA-Z0-9\-\_\.]+@[a-zA-Z0-9\-\_]+(\.[a-zA-Z0-9]{2,4}){1,2}$/", $value) !== false;
            case "password":
                return preg_match("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/", $value) !== false;
            case "date":
                return strtotime($value) !== false;
            default:
                return strlen($value) > 1;
        }
    }

    public static function password($password)
    {
        $h1 = md5($password);
        $h2 = sha1($password);
        return md5($h1 . sha1($h1 . $h2) . $h2);
    }

    public static function generateKey()
    {
        return md5(sha1(time()));
    }

    public static function lastId($name)
    {
        return self::getInstance()->lastInsertId($name);
    }

    public static function migrate()
    {
        print_r("Migration started...\n");

        $migrations = array_filter(scandir(self::$migrationsPath), function ($migration) {
            return preg_match("/.*\.sql$/", $migration);
        });

        foreach ($migrations as $migration) {
            $src = self::$migrationsPath . "/{$migration}";
            $dst = self::$migrationsPath . "/executed/{$migration}";
            if (file_exists($dst)) {
                print_r("{$src} is skipped.\n");
                continue;
            }
            if (false === self::rawQuery(file_get_contents($src))) {
                $error = self::getInstance()->errorInfo();
                print_r("{$src} returns error:\n");
                print_r("SQLSTATE: {$error[0]}\n");
                print_r("Error Code: {$error[1]}\n");
                print_r("Error Message: {$error[2]}\n");
                return false;
            }
            print_r("{$src} is migrated.\n");
            copy($src, $dst);
        }

        print_r("Migration finished successfully.\n");
        return true;
    }

    private static function rawQuery($sql)
    {
        return self::getInstance()->query($sql);
    }

    private static function makePairs($data)
    {
        $pdo = self::getInstance();
        $conditions = [];

        foreach ($data as $col => $val) {
            $conditions[] = "{$col}={$pdo->quote($val)}";
        }

        return $conditions;
    }
}
