<?php

namespace App\Common;

class Response
{
    private static $instance;

    protected $code = 400;
    protected $message;
    protected $payload;

    private function __construct() {}
    private function __clone() {}

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    function internalError()
    {
        $this->code = 500;
        $this->message = "Internal Server Error";
        $this->go();
    }

    function notFound()
    {
        $this->code = 404;
        $this->message = "page not found";
        $this->go();
    }

    function resOk($data = [], $message = null, $code = 200)
    {
        $this->code = $code;
        $this->message = $message;
        $this->payload = $data;
        $this->go();
    }

    function unAuth()
    {
        $this->code = 401;
        $this->message = "Unauthorized";
        $this->go();
    }

    function forbidden()
    {
        $this->code = 403;
        $this->message = "Not allowed";
        $this->go();
    }

    function badRequest($message = "Bad Request", $code = 400)
    {
        $this->code = $code;
        $this->message = $message;
        $this->go();
    }

    private function go()
    {
        http_response_code($this->code);
        header("Content-Type: application/json");
        $response = [
            "message" => $this->message,
            "payload" => $this->payload
        ];
        echo json_encode($response);
        exit;
    }
}
