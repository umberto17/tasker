<?php

namespace App\Common;

class App {
    public static function go()
    {
        $request = Request::getInstance();
        $response = Response::getInstance();
        $controllerClass = "App\\Controllers\\{$request->object}Controller";
        if (class_exists($controllerClass)) {
            $controller = new $controllerClass();
            $method = ($request->method === "get" && !$request->id) ? "getAll" : $request->method;
            if (method_exists($controller, $method)) {
                $controller->$method();
            } else {
                $response->badRequest();
            }
        } else {
            $response->notFound();
        }
    }
}
