<?php

namespace App\Models;

use App\Common\DB;
use App\Common\Request;

class Task {
    private $selectAllSql = "select id, date, title, description, completed from tasks where user_id=? order by date asc";
    private $selectOneSql = "select id, date, title, description, completed from tasks where id=? and user_id=? order by date asc";
    private $insertSql = "insert into tasks (date, title, description, completed, user_id) values (?, ?, ?, ?, ?)";
    private $updateSql = "update tasks set date=?, title=?, description=?, completed=? where id=? and user_id=?";
    private $deleteSql = "delete from tasks where id=? and user_id=?";

    function getAll()
    {
        $request = Request::getInstance();
        $user = $request->currentUser;
        $data = [$user->id];
        return DB::getAll($data, $this->selectAllSql);
    }

    function get()
    {
        $request = Request::getInstance();
        $user = $request->currentUser;
        $data = [$request->id, $user->id,];
        return DB::get($data, $this->selectOneSql);
    }

    function insert($data)
    {
        return DB::insert($data, $this->insertSql);
    }

    function update($data) {
        $request = Request::getInstance();
        $id = $request->id;
        $user = $request->currentUser;
        $data['id'] = $id;
        $data['user_id'] = $user->id;
        return DB::update($data, $this->updateSql);
    }

    function delete()
    {
        $request = Request::getInstance();
        $id = $request->id;
        $user = $request->currentUser;
        $data = [$id, $user->id];
        return DB::delete($data, $this->deleteSql);
    }
}