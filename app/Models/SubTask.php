<?php

namespace App\Models;

use App\Common\DB;
use App\Common\Request;

class SubTask
{
    private $selectRelatedSql = "select id, title, completed, task_id from sub_tasks where task_id in (:?:)";
    private $insertSql = "insert into sub_tasks (title, completed, task_id) values (?, ?, ?)";
    private $updateSql = "update sub_tasks left outer join tasks on tasks.id=sub_tasks.task_id set sub_tasks.title=?, sub_tasks.completed=?, sub_tasks.task_id=? where sub_tasks.id=? and tasks.user_id=?";
    private $deleteSql = "delete sub_tasks from sub_tasks left join tasks on tasks.id=sub_tasks.task_id where sub_tasks.id=? and tasks.user_id=?";

    function joinSubtasks(array $tasks)
    {
        $ids = $this->extractIds($tasks);
        $values = implode(",", array_fill(0, count($ids), "?"));
        $subtasks = DB::getAll($ids, str_replace(":?:", $values, $this->selectRelatedSql));
        return $this->implodeTasks($tasks, $subtasks);
    }

    function insert($data)
    {
        return DB::insert($data, $this->insertSql);
    }

    function update($data, $id = false)
    {
        $request = Request::getInstance();
        $data["id"] = $id ? $id : $request->id;
        $data["user_id"] = $request->currentUser->id;
        return DB::update($data, $this->updateSql);
    }

    function delete($id = false)
    {
        $request = Request::getInstance();
        $data = [$id ? $id : $request->id,  $request->currentUser->id];
        return DB::delete($data, $this->deleteSql);
    }

    private function implodeTasks($tasks, $subtasks)
    {
        $result = [];
        array_walk($tasks, function ($task) use (&$result) {
            $task["subtasks"] = [];
           $result[$task["id"]] = $task;
        });
        array_walk($subtasks, function ($subtask) use (&$result) {
            if (isset($result[$subtask["task_id"]])) {
                $task = &$result[$subtask["task_id"]];
                $task["subtasks"][] = $subtask;
            }
        });
        return array_values($result);
    }

    private function extractIds($tasks)
    {
        return array_map(function ($task) {return $task["id"];}, $tasks);
    }
}