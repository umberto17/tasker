<?php

namespace App\Models;

use App\Common\DB;

class User {

    private $insertSql = "insert into users (email, password) values (?, ?)";
    private $selectByEmailPwdSql = "select id, email, pkey from users where email=? and password=?";
    private $selectByKeySql = "select id, email, pkey from users where pkey=?";
    private $selectEmailSql = "select email from users where email=?";
    private $updateKeySql = "update users set pkey=? where id=?";

    function getByEmailPwd($email, $pwd)
    {
        $data = [
            $email,
            DB::password($pwd)
        ];
        return DB::get($data, $this->selectByEmailPwdSql);
    }

    function getByKey($key)
    {
        $data = [$key];
        return DB::get($data, $this->selectByKeySql);
    }

    function checkForEmail($email)
    {
        $data = [$email];
        if ($res = DB::get($data, $this->selectEmailSql)) {
            return count($res);
        }
        return false;
    }

    function insert($data) {
        return DB::insert($data, $this->insertSql);
    }

    function updateKey($user_id, $key = "")
    {
        $data = [$key, $user_id];
        return DB::update($data, $this->updateKeySql);
    }
}