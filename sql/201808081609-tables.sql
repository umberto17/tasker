create table if not exists `users` (
  `id` int(11) primary key auto_increment not null,
  `email` varchar(50) not null,
  `password` varchar(50) not null,
  `pkey` varchar(50) not null
) engine=innodb default charset=utf8;

create table if not exists `tasks` (
  `id` int(11) primary key auto_increment not null,
  `date` date not null,
  `title` varchar(50) not null,
  `description` text not null,
  `completed` tinyint(1) not null,
  `user_id` int(11) not null,
  index tasks_user_id_idx (user_id),
  constraint tasks_users_user_id_fk foreign key (user_id)
      references users(id)
      on delete cascade
) engine=innodb default charset=utf8;
