create table if not exists `sub_tasks` (
  `id` int(11) primary key auto_increment not null,
  `title` varchar(50) not null,
  `completed` tinyint(1) not null,
  `task_id` int(11) not null,
  index sub_tasks_task_id_idx (`task_id`),
  constraint sub_tasks_tasks_task_id_fk foreign key (`task_id`)
      references `tasks`(`id`)
      on delete cascade
) engine=innodb default charset=utf8;
