import React, {Component} from "react";
import {connect} from "react-redux";

import {AppBar, FlatButton} from "material-ui";

import {openDialog, logout} from "./../stores/app";

class HeaderContainer extends Component {
    renderLoginButton = () => {
      const {user, openDialog, logout} = this.props;
      if (user) {
          return <FlatButton label="Logout" onTouchTap={logout} />
      }
      return <FlatButton label="Login" onTouchTap={() => openDialog("login")}/>;
    };

    render() {
        return <AppBar title="Tasker"
                       iconElementRight={this.renderLoginButton()}/>;
    }
}

const mapStateToProps = state => {
  return {
      user: state.app.user
  }
};

export default connect(mapStateToProps, {openDialog, logout})(HeaderContainer);
