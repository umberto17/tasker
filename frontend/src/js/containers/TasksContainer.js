import React, {Component} from "react";
import {connect} from "react-redux";

import {
  Subheader,
  Table,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableBody,
  TableRowColumn,
  FloatingActionButton,
  RaisedButton
} from "material-ui";
import ContentAdd from 'material-ui/svg-icons/content/add';
import {openDialog, deleteTask} from "./../stores/app";

const columns = [
  {title: "Date", name: "date"},
  {title: "Title", name: "title"},
  {title: "Description", name: "description"},
  {title: "Subtasks Number", name: "sub_tasks", render: task => task.subtasks.length}
];

class TasksContainer extends Component {

  popupLink = (text, action) => {
    const style = {
      color: "black",
      textDecoration: "underline",
      cursor: "pointer"
    };
    return <span style={style} onTouchTap={action}>{text}</span>
  };

  renderTableRow = (task, i) => {
    const {deleteTask, openDialog} = this.props;
    return <TableRow key={i} selected={!!+task.completed}>
        {columns.map((col, i) => <TableRowColumn key={i}>
          {col.render ? col.render(task) : task[col.name]}
          </TableRowColumn>)}
        <TableRowColumn>
          <RaisedButton primary={true} label="Edit" onTouchTap={() => openDialog("editTask", task)}/>
          <RaisedButton secondary={true} label="Delete" onTouchTap={() => deleteTask(task.id)}/>
        </TableRowColumn>
    </TableRow>;
  };

  renderTableBody = () => {
    const {tasks} = this.props;
    return <TableBody deselectOnClickaway={false}>
      {tasks.map((task, i) => this.renderTableRow(task, i))}
    </TableBody>;
  };

  renderTableHeader = () => {
    return <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
      <TableRow>
        {columns.map((col, i) => <TableHeaderColumn key={i}>{col.title}</TableHeaderColumn>)}
        <TableHeaderColumn key={columns.length}/>
      </TableRow>
    </TableHeader>
  };

  renderTable = () => {
    return <Table  multiSelectable={true}>
      {this.renderTableHeader()}
      {this.renderTableBody()}
    </Table>
  };

  renderList = () => {
    return <div>
      <Subheader>Tasks</Subheader>
      {this.renderTable()}
      {this.renderAddButton()}
    </div>;
  };

  renderEmpty = () => {
    const {openDialog} = this.props;
    return <Subheader>
      Please {this.popupLink("Login", () => openDialog("login"))} or {this.popupLink("Register", () => openDialog("registration"))} to manage your tasks
    </Subheader>
  };

  renderAddButton = () => {
    const {openDialog} = this.props;
    const style = {
      position: "fixed",
      right: "30px",
      bottom: "30px"
    };
    return <FloatingActionButton style={style} onTouchTap={() => openDialog("newTask")}>
      <ContentAdd />
    </FloatingActionButton>
  };

  render() {
    const {user} = this.props;
    return user ? this.renderList() : this.renderEmpty();
  }
}

const mapStateToProps = state => {
  return {
    user: state.app.user,
    tasks: state.app.tasks
  };
};

export default connect(mapStateToProps, {openDialog, deleteTask})(TasksContainer);
