import React, {Component} from "react";
import {connect} from "react-redux";

import {RefreshIndicator, Snackbar} from "material-ui";

import TasksContainer from "./TasksContainer";
import DialogContainer from "./DialogContainer";

import {snackbarHide} from "./../stores/snackbar";

class MainContainer extends Component {
  render() {
    const {is_loading, snackMessage, snackOpened, snackbarHide} = this.props;
    const style = {
      overlay: {
        position: 'absolute',
        left: '0',
        top: '0',
        width: '100%',
        height: '100%',
        background: 'rgba(0, 0, 0, .5)',
        zIndex: '15000'
      },
      indicator: {
        display: 'inline-block',
        position: 'relative',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
      }
    };
    return <div>
      <TasksContainer/>
      <DialogContainer/>
      {is_loading && <div style={style.overlay}>
        <RefreshIndicator
          size={40}
          left={20}
          top={20}
          status="loading"
          style={style.indicator}
        />
      </div>}
      <Snackbar
        open={snackOpened || false}
        message={snackMessage || ''}
        autoHideDuration={3000}
        action="ok"
        onActionTouchTap={snackbarHide}
        onRequestClose={snackbarHide}
      />
    </div>;
  }
}

const mapStateToProps = state => {
  return {
    is_loading: state.app.is_loading,
    user: state.app.user,
    snackOpened: state.snackbar.opened,
    snackMessage: state.snackbar.message
  };
};

export default connect(mapStateToProps, {snackbarHide})(MainContainer);
