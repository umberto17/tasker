import React, {Component} from "react";

import HeaderContainer from "./HeaderContainer";
import MainContainer from "./MainContainer";

class AppContainer extends Component {
    render() {
        return <div>
          <HeaderContainer />
          <MainContainer />
        </div>;
    }
}

export default AppContainer;
