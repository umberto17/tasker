import React, {Component} from "react";
import {connect} from "react-redux";

import {
  Dialog,
  TextField,
  RaisedButton,
  DatePicker,
  Checkbox,
  Subheader,
  IconButton,
  FontIcon
} from "material-ui";

import ClearIcon from "material-ui/svg-icons/content/clear";
import AddIcon from "material-ui/svg-icons/content/add";

import {Grid, Row, Col} from "react-bootstrap";
import {
  openDialog,
  closeDialog,
  login,
  register,
  addTask,
  updateTask
} from "../stores/app";

import {dateFormat} from "./../utils/datetime";

const renderSubtasks = (subtask, setState, subtasks, i) => {
  if (subtask.delete) {
    return null;
  }

  return <li className="inline">
    <div>
      <Checkbox
        checked={+subtask.completed}
        onCheck={e => {
          subtasks[i].completed = e.target.checked;

          setState({subtasks});
        }}
      />
    </div>
    <div>
      <input
        className="edit-field"
        onChange={e => {
          subtasks[i].title = e.target.value;
          setState({subtasks});
        }}
        value={subtask.title}
      />
    </div>
    <div>
      <IconButton
        onTouchTap={() => {
          subtasks[i].delete = true;
          setState({subtasks});
        }}><ClearIcon/></IconButton>
    </div>
  </li>
};

const emptySubtask = {
  title: "Subtask",
  completed: 0
};

const fields = {
  login: {
    title: "Login",
    buttonLabel: "Login",
    buttonAction: "login",
    sendId: false,
    fields: [
      {
        name: "email",
        type: "email",
        title: "Email",
        errorText: "Type valid email"
      },
      {
        name: "password",
        type: "password",
        title: "Password",
        errorText: "Minimum 6 characters, at least one letter and one number"
      }
    ],
  },
  registration: {
    title: "Registration",
    buttonLabel: "Register",
    buttonAction: "register",
    sendId: false,
    fields: [
      {
        name: "email",
        type: "email",
        title: "Email",
        errorText: "Type valid email"
      },
      {
        name: "password",
        type: "password",
        title: "Password",
        errorText: "Password must contains lowercase letter, uppercase letter and digit."
      },
      {
        name: "password_confirm",
        type: "password",
        validationType: "password_confirm",
        title: "Confirmation Password",
        errorText: "Passwords must be matches"
      }
    ],
  },
  newTask: {
    title: "New Task",
    buttonLabel: "Add Task",
    buttonAction: "addTask",
    sendId: false,
    columns: 2,
    fields: [
      {
        name: "date",
        type: "date",
        title: "Date",
        errorText: "Set Date"
      },
      {
        name: "title",
        type: "text",
        title: "Title",
        errorText: "Couldn't be empty"
      },
      {
        name: "description",
        type: "textarea",
        title: "Description",
        errorText: "Couldn't be empty"
      },
      {
        name: "subtasks",
        type: "list",
        title: "Subtasks",
        emptyItem: emptySubtask,
        empty: [],
        column: 1,
        render: renderSubtasks
      }
    ],
  },
  editTask: {
    title: "Edit Task",
    buttonLabel: "Update Task",
    buttonAction: "updateTask",
    sendId: true,
    columns: 2,
    fields: [
      {
        name: "date",
        type: "date",
        title: "Date",
        errorText: "Set Date"
      },
      {
        name: "title",
        type: "text",
        title: "Title",
        errorText: "Couldn't be empty"
      },
      {
        name: "description",
        type: "textarea",
        title: "Description",
        errorText: "Couldn't be empty"
      },
      {
        name: "completed",
        type: "checkbox",
        title: "Completed",
        errorText: "",
        shouldAppear: state => !state.subtasks || !state.subtasks.filter(subtask => !subtask.delete).length
      },
      {
        name: "subtasks",
        type: "list",
        title: "Subtasks",
        emptyItem: emptySubtask,
        render: renderSubtasks,
        column: 1
      }
    ],
  }

};

class DialogContainer extends Component {

  constructor() {
    super();
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {
    const {show} = this.props;
    const object = nextProps.object || null;
    if (!show && nextProps.show) {
      if (fields[nextProps.type]) {
        this.generateState(fields[nextProps.type].fields, object);
      }
    }
  }

  generateState = (fields, object) => {
    const state = {
      submitDisabled: true
    };
    fields.map(field => state[field.name] = object ? this.handleType(field.type, object[field.name]) : (field.empty || ""));
    fields.map(field => state[field.name + "Error"] = "");
    this.setState(state);
  };

  handleType = (type, value) => {
    switch (type) {
      case "date":
        return new Date(value);
      case "checkbox":
        return !!+value;
      default:
        return value;
    }
  };

  validate = (type, value) => {
    if (!value.length) {
      return false;
    }

    switch (type) {
      case "email":
        return ~value.search(/^[a-zA-Z0-9\-\_\.]+@[a-zA-Z0-9\-\_]+(\.[a-zA-Z0-9]{2,4}){1,2}$/);
      case "password":
        return ~value.search(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/);
      case "password_confirm":
        return value === this.state.password
      case "text":
        return value.length;
      case "date":
        const today = new Date();
        return value > today.setDate(today.getDate() + 1)
    }
    return true;
  };

  typingText = (field, value) => {
    const state = {
      submitDisabled: false
    };
    const {name, type, validationType, errorText} = field;
    state[name + "Error"] = "";
    if (!this.validate(validationType || type, value)) {
      state[name + "Error"] = errorText;
      state.submitDisabled = true;
    }
    state[name] = value;
    this.setState(state);
  };

  renderTextField = (field, rows) => {
    const {title, type, name} = field;
    const value = this.state[name];
    const error = this.state[name + "Error"];
    return <div><TextField type={type}
                           floatingLabelText={title}
                           fullWidth={true}
                           value={value}
                           onChange={e => this.typingText(field, e.target.value)}
                           rows={rows || 1}
                           errorText={error}/></div>;
  };

  renderCheckbox = (field) => {
    const {title, name} = field;
    const checked = this.state[name];
    return (field.shouldAppear && field.shouldAppear(this.state)) ?
      <Checkbox
        checked={checked}
        label={title}
        onCheck={e => this.handleCheck(field, e)}
      /> :
      null;
  };

  handleCheck = (field, e) => {
    const state = {};
    state[field.name] = e.target.checked;
    this.setState(state);
  };

  renderDate = (field) => {
    const {title, name} = field;
    const value = this.state[name];
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    return <DatePicker autoOk={true}
                       onChange={(undefined, date) => this.typingText(field, date)}
                       minDate={tomorrow}
                       value={value}
                       placeholder={title}/>;
  };

  renderSubmitButton = (label) => {
    const {submitDisabled} = this.state;
    return <div><RaisedButton label={label}
                              onTouchTap={this.doSubmit}
                              primary={true}
                              disabled={submitDisabled}/></div>;
  };

  doSubmit = () => {
    const {type, object} = this.props;
    const id = object ? object.id : null;
    const action = this.props[fields[type].buttonAction];
    const data = {};
    fields[type].fields.map(field => {
      let value = this.state[field.name];
      switch (field.type) {
        case "date":
          value = dateFormat(value);
          break;
        case "checkbox":
          value = value ? 1 : 0;
          break;
      }
      data[field.name] = value;
    });
    action(data, id);
  };

  renderColumns = fields => {
    const columns = [];
    fields.forEach(field => {
      const colNum = field.column || 0;
      if (!columns[colNum]) {
        columns[colNum] = [];
      }
      columns[colNum].push(field);
    });
    return columns;
  };

  renderContent = () => {
    const {type} = this.props;
    fields[type].columns = fields[type].columns || 1;
    const columns = this.renderColumns(fields[type].fields);
    const colSize = Math.round(12 / columns.length);
    return <Grid fluid={true}>
      <Row>
        {columns.map((fields, i) => (
          <Col sm={colSize} key={i}>
            {fields.map((field) => this.renderField(field))}
          </Col>))}
      </Row>
    </Grid>;
  };

  renderList = field => {
    const items = [...this.state[field.name]];
    return <div>
      <Subheader>{field.title}:
        <IconButton
          onTouchTap={() => {
            const newItem = {...field.emptyItem};
            newItem.title += " " + (items.length + 1);
            items.push(newItem);
            const state = {};
            state[field.name] = items;
            this.setState(state);
          }}><AddIcon/></IconButton>
      </Subheader>
      <div style={{float: "left"}}>
      </div>
      <ul className={field.name}>
        {items.map((item, i) => (
            field.render ? field.render(item, state => this.setState(state), items, i) : item
          )
        )}
      </ul>
    </div>;
  };

  renderField = (field) => {
    switch (field.type) {
      case "email":
      case "password":
      case "text":
        return this.renderTextField(field);
      case "textarea":
        return this.renderTextField(field, 5);
      case "date":
        return this.renderDate(field);
      case "checkbox":
        return this.renderCheckbox(field);
      case "list":
        return this.renderList(field);
    }
  };

  renderTitle = fields => {
    if (!fields) {
      return null;
    }
    const style = {display: "inline-block", marginRight: "10px"};
    return <div>
      <div style={style}>{fields.title}</div>
      <div style={style}>{this.renderSubmitButton(fields.buttonLabel)}</div>
    </div>;
  };

  render() {
    const {type, show, closeDialog} = this.props;

    return <Dialog
      open={show}
      onRequestClose={closeDialog}
      bodyStyle={{overflow: "auto"}}
      title={this.renderTitle(fields[type])}
    >
      {fields[type] && this.renderContent()}
    </Dialog>;
  }
}

const mapStateToProps = state => {
  return {
    show: state.app.dialogShow,
    type: state.app.dialogType,
    object: state.app.object
  };
};

const mapDispatchToProps = {
  login,
  register,
  openDialog,
  closeDialog,
  addTask,
  updateTask
};

export default connect(mapStateToProps, mapDispatchToProps)(DialogContainer);
