import { combineReducers } from 'redux'
import app from "./app";
import snackbar from "./snackbar";

const reducers = combineReducers({
  app,
  snackbar
});

export default reducers