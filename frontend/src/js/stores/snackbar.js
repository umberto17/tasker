const initialState = {
  opened: false,
  message: null
};

const SNACKBAR_SHOW = 'snackbar_show';
const SNACKBAR_HIDE = 'snackbar_hide';

export function snackbarShow(message) {
  return function (dispatch) {
    dispatch({type: SNACKBAR_SHOW, payload: message})
  }
}

export function snackbarHide() {
  return function (dispatch) {
    dispatch({type: SNACKBAR_HIDE})
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SNACKBAR_SHOW:
      return {...state, opened: true, message: action.payload};
    case SNACKBAR_HIDE:
      return {...state, opened: false, message: null};

    default:
      return state;
  }
}