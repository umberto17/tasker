import ajax from "./../utils/ajax";

import {snackbarShow} from "./snackbar";

const initialState = {
  dialogShow: false,
  dialogType: null,
  user: null,
  tasks: [],
  message: null,
  snackBarShow: false,
  object: null
};

const SHOW_DIALOG = "show_dialog";
const HIDE_DIALOG = "hide_dialog";

const LOGIN_LOADING = "login_loading";
const LOGIN_LOADED = "login_loaded";
const LOGIN_ERROR = "login_error";

const LOGOUT = "logout";

const REGISTER_LOADING = "register_loading";
const REGISTER_LOADED = "register_loaded";
const REGISTER_ERROR = "register_error";

const RETRIEVE_USER_LOADING = "retrieve_user_loading";
const RETRIEVE_USER_LOADED = "retrieve_user_loaded";
const RETRIEVE_USER_ERROR = "retrieve_user_error";

const RETRIEVE_TASKS_LOADING = "retrieve_tasks_loading";
const RETRIEVE_TASKS_LOADED = "retrieve_tasks_loaded";
const RETRIEVE_TASKS_ERROR = "retrieve_tasks_error";

const ADD_TASK_LOADING = "add_task_loading";
const ADD_TASK_LOADED = "add_task_loaded";
const ADD_TASK_ERROR = "add_task_error";

const UPDATE_TASK_LOADING = "update_task_loading";
const UPDATE_TASK_LOADED = "update_task_loaded";
const UPDATE_TASK_ERROR = "update_task_error";

const DELETE_TASK_LOADING = "delete_task_loading";
const DELETE_TASK_LOADED = "delete_task_loaded";
const DELETE_TASK_ERROR = "delete_task_error";

export function openDialog(type, object) {
  return function (dispatch) {
    dispatch({type: SHOW_DIALOG, payload: {type, object}});
  }
}

export function closeDialog() {
  return function (dispatch) {
    dispatch({type: HIDE_DIALOG});
  }
}

export function login(data) {
  return function (dispatch, getState) {
    dispatch({type: LOGIN_LOADING});
    ajax("post", "login", null, data)
      .then(res => {
        const {user} = res.data.payload;
        dispatch({type: LOGIN_LOADED, payload: user});
        localStorage.setItem("pkey", user.key);
        closeDialog()(dispatch);
        getTasks()(dispatch, getState);
      })
      .catch(err => dispatch({type: LOGIN_ERROR}));
  }
}

export function logout() {
  return function (dispatch) {
    localStorage.removeItem("pkey");
    dispatch({type: LOGOUT});
  }
}

export function register(data) {
  return function (dispatch) {
    dispatch({type: REGISTER_LOADING});
    ajax("post", "user", null, data)
      .then(res => {
        snackbarShow(res.data.message)(dispatch);
        openDialog("login")(dispatch);
        dispatch({type: REGISTER_LOADED});
      })
      .catch(err => {
        console.log(err);
        // snackbarShow(err.response.data.message)(dispatch);
        dispatch({type: REGISTER_ERROR});
      });
  }
}

export function getTasks() {
  return function (dispatch, getState) {
    dispatch({type: RETRIEVE_TASKS_LOADING});
    const state = getState();
    if (state.app.user) {
      ajax("get", "task")
        .then(res => dispatch({type: RETRIEVE_TASKS_LOADED, payload: res.data.payload}))
        .catch(err => dispatch({type: RETRIEVE_TASKS_ERROR, payload: err.data}));
    } else {
      dispatch({type: RETRIEVE_TASKS_ERROR, payload: {message: "Not Logged In"}});
    }
  }
}

export function addTask(data) {
  return function (dispatch, getState) {
    dispatch({type: ADD_TASK_LOADING});
    ajax("post", "task", null, data)
      .then(res => {
        snackbarShow(res.data.message)(dispatch);
        closeDialog()(dispatch);
        getTasks()(dispatch, getState);
        dispatch({type: ADD_TASK_LOADED});
      })
      .catch(err => {
          snackbarShow(err.response.data.message)(dispatch);
          dispatch({type: ADD_TASK_ERROR, payload: err.data})
        }
      );
  }
}

export function updateTask(data, id) {
  return async function (dispatch, getState) {
    dispatch({type: UPDATE_TASK_LOADING});
    try {
      const res = await ajax("put", "task", id, data);
      dispatch({type: UPDATE_TASK_LOADED});
      snackbarShow(res.data.message)(dispatch);
      closeDialog()(dispatch);
      getTasks()(dispatch, getState);
    } catch (err) {
      dispatch({type: UPDATE_TASK_ERROR, payload: err.data})
    }
  }
}

export function deleteTask(id) {
  return function (dispatch, getState) {
    dispatch({type: DELETE_TASK_LOADING});
    ajax("delete", "task", id)
      .then(res => {
        dispatch({type: DELETE_TASK_LOADED});
        snackbarShow(res.data.message)(dispatch);
        getTasks()(dispatch, getState);
      })
      .catch(err => dispatch({type: DELETE_TASK_ERROR, payload: err.data}));
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SHOW_DIALOG:
      return {...state, dialogShow: true, dialogType: action.payload.type, object: action.payload.object};
    case HIDE_DIALOG:
      return {...state, dialogShow: false, dialogType: null, object: null};

    case LOGOUT:
      return {...state, user: null, tasks: []};

    case LOGIN_LOADING:
      return {...state, is_loading: true, user: null, error: null};
    case LOGIN_LOADED:
      return {...state, is_loading: false, user: action.payload, error: null};
    case LOGIN_ERROR:
      return {...state, is_loading: false, user: [], error: action.payload};

    case RETRIEVE_TASKS_LOADING:
      return {...state, is_loading: true, error: null};
    case RETRIEVE_TASKS_LOADED:
      return {...state, is_loading: false, tasks: action.payload, error: null};
    case RETRIEVE_TASKS_ERROR:
      return {...state, is_loading: false, tasks: [], error: action.payload};

    case REGISTER_LOADING:
    case ADD_TASK_LOADING:
    case UPDATE_TASK_LOADING:
    case DELETE_TASK_LOADING:
      return {...state, is_loading: true, error: null};
    case REGISTER_LOADED:
    case ADD_TASK_LOADED:
    case UPDATE_TASK_LOADED:
    case DELETE_TASK_LOADED:
      return {...state, is_loading: false, error: null};
    case REGISTER_ERROR:
    case ADD_TASK_ERROR:
    case UPDATE_TASK_ERROR:
    case DELETE_TASK_ERROR:
      return {...state, is_loading: false, error: action.payload};

    default:
      return state;
  }
}