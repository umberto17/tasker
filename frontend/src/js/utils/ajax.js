import axios from 'axios';
import {SERVER_URL} from './../config';

export default function (method, call, id = null, data = null) {
  let url = SERVER_URL + "/" + call;
  if (id) url += "/" + id;
  const params = {
    method,
    baseURL: url,
    headers: {
      'Authorization': localStorage.getItem('pkey')
    },
    data: data
  };
  return axios(params);
}