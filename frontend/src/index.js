import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducers from './js/stores/reducers';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import './index.css';
import AppContainer from './js/containers/AppContainer';
import {BrowserRouter} from "react-router-dom";

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

const store = createStore(reducers, applyMiddleware(thunk));

render(
  <BrowserRouter>
    <Provider store={store}>
      <MuiThemeProvider>
        <AppContainer/>
      </MuiThemeProvider>
    </Provider>
  </BrowserRouter>, document.getElementById('app'));
