# Installation Guide.

1. Git clone to `<directory>`.
2. Configure Apache. Set `<directoty>/public` as DocumentRoot.
3. Create database by query in `<directory>/sql/database.sql`. One time only.
4. Make migration `php app/migrate.php`
4. Copy `<directory>/app/config.sample.php` to `config.php` same directory.
5. Update `<directoty>/app/config.php`, set constants according your server.
6. Copy `<directory>/fronend/src/js/config.sample.js` to `config.php` same directory.
7. Update `<directoty>/frontend/src/js/config.js`, set constants according your server.
8. Go to `<directory>/frontend`.
9. Run 
```
npm install
```
10. Run 
```
npm run build
```
11. Copy all files from `<directory>/frontend/build` to `<directory>/public/`.

12. Open in browser `http://<your-server>/`

Note: Be sure your apache allows overriding.
